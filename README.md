# Python Ninja Syntax

This package simply pulls the [latest version][1] of `misc/ninja_syntax.py` from the
[Ninja source code repository][2].

[1]: https://raw.githubusercontent.com/ninja-build/ninja/master/misc/ninja_syntax.py
[2]: https://github.com/ninja-build/ninja
