import setuptools
import urllib.request

with urllib.request.urlopen('https://raw.githubusercontent.com/ninja-build/ninja/master/misc/ninja_syntax.py') as f:
    content = f.read().decode('utf-8')

with open('ninja_syntax/__init__.py', 'w') as f:
    f.write(content)

setuptools.setup(
    name = 'ninja_syntax',
    version = '0.1.0',
    description = ("misc/ninja_syntax.py pulled from the Ninja source code repository"),
    packages = ['ninja_syntax'],
)
